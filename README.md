# BTT



## About

This is a Tech Task solution

## Project Setup

```
clone this project
cd to this project folder

! don't forget to create and activate you virtual environment

install requirements:
 - pip install -r requirements.txt

run project:
 - uvicorn main:app --host 0.0.0.0 --port 5000 --reload

start populating/searching generator:
 - ./generator -submit-url http://127.0.0.1:5000/submit
 - ./generator -search-url http://127.0.0.1:5000/search

swagger docs:
 - http://127.0.0.1:5000/docs

run tests:
 - pytest tests.py
```
