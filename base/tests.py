import pytest
from fastapi.testclient import TestClient

from base.urls import main_router

client = TestClient(main_router)


@pytest.fixture
def sample_submission():
    submission_data = {
        "title": "Test Title",
        "author": "Test Author",
        "text": "Test Text",
        "url": "https://example.com"
    }
    response = client.post("/submit", json=submission_data)
    assert response.status_code == 201
    yield submission_data


def test_submit_submission(sample_submission):
    pass  # No need for additional tests as the fixture already covers submission creation


def test_search_submissions(sample_submission):
    response = client.get("/search?title=st+Title&author=Test+Author&size=10")
    assert response.status_code == 200
    submissions = response.json()
    assert isinstance(submissions, list)
    assert len(submissions) > 1


def test_get_submission(sample_submission):
    submission_id = 1
    response = client.get(f"/item/{submission_id}")
    assert response.status_code == 200
    submission = response.json()
    assert isinstance(submission, dict)
    assert submission["id"] == submission_id


def test_get_rss_feed(sample_submission):
    response = client.get("/rss?title=st+Title&author=Test+Author&size=10")
    assert response.status_code == 200
