from datetime import datetime
from hashlib import md5
from typing import List, Optional, Annotated
from fastapi import HTTPException, APIRouter, Header
from starlette.responses import Response

from base.models import SubmissionIn, Submission, submissions, SubmissionOut
from base.scripts import filter_data, convert_list_of_dicts_to_xml

main_router = APIRouter(
    prefix="",
    tags=["main"],
    responses={404: {"description": "Not found"}},
)


@main_router.post("/submit", status_code=201)
async def submit_submission(data: SubmissionIn, user_agent: Annotated[str | None, Header()] = None):
    # Generate additional fields
    submission = Submission(
        id=str(len(submissions) + 1),
        created_at=datetime.now().isoformat(),
        title_md5=md5(data.title.encode()).hexdigest(),
        text_md5=md5(data.text.encode()).hexdigest(),
        title_len_ch=len(data.title),
        text_len_ch=len(data.text),
        title_len_words=len(data.title.split()),
        text_len_words=len(data.text.split()),
        url_hostname=data.url.split("//")[-1].split("/")[0],
        url_scheme=data.url.split(":")[0],
        submitted_by=user_agent,
        **data.dict()
    )
    submissions.append(submission)
    return


@main_router.get("/search", response_model=List[SubmissionOut])
async def search_submissions(title: Optional[str] = "", author: Optional[str] = "", size: int = 50):
    return filter_data(title=title, author=author, size=size)


@main_router.get("/item/{submission_id}", response_model=Submission)
def get_submission(submission_id: int):
    for submission in submissions:
        if submission.id == submission_id:
            return submission
    raise HTTPException(status_code=404, detail="Submission not found")


@main_router.get("/rss", response_model=str)
def get_rss_feed(title: Optional[str] = "", author: Optional[str] = "", size: int = 50):
    feed = convert_list_of_dicts_to_xml(filter_data(title=title, author=author, size=size))
    return Response(feed, media_type='application/rss+xml')
