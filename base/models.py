from pydantic import BaseModel

submissions = []


class SubmissionIn(BaseModel):
    title: str
    author: str
    text: str
    url: str


class SubmissionOut(BaseModel):
    id: int
    title: str
    author: str
    text: str
    created_at: str


class Submission(BaseModel):
    id: int
    title: str
    author: str
    text: str
    url: str
    created_at: str
    title_md5: str
    text_md5: str
    title_len_ch: int
    text_len_ch: int
    title_len_words: int
    text_len_words: int
    url_hostname: str
    url_scheme: str
    submitted_by: str
