from xml.dom import minidom

from fuzzywuzzy import fuzz
import xml.etree.ElementTree as et

from base.models import submissions, SubmissionOut


def fuzzy_match(title: str, submission_title: str, ratio: int = 10) -> bool:
    if not title:
        return True
    return fuzz.partial_ratio(title, submission_title) >= ratio


def exact_match(author: str, submission_author: str) -> bool:
    if not author:
        return True
    return author.lower() == submission_author.lower()


def format_string(data: str) -> str:
    return data.replace("+", " ").lower()


def filter_data(size, title, author):
    filtered_submissions = []

    for submission in reversed(submissions):
        if len(filtered_submissions) == size:
            break

        if not title and not author:
            filtered_submissions.append(SubmissionOut(**submission.dict()))
            continue

        if not fuzzy_match(format_string(title), format_string(submission.title), 60):
            continue

        if not exact_match(format_string(author), format_string(submission.author)):
            continue

        filtered_submissions.append(SubmissionOut(**submission.dict()))
    return filtered_submissions


def convert_list_of_dicts_to_xml(data_list):
    root = et.Element('root')

    for item in data_list:
        entry = et.SubElement(root, 'entry')
        for key, value in item.dict().items():
            field = et.SubElement(entry, key)
            field.text = str(value)

    xml_data = et.tostring(root, encoding='utf-8', xml_declaration=True)
    xml_dom = minidom.parseString(xml_data)
    pretty_xml_str = xml_dom.toprettyxml(indent="  ")

    return pretty_xml_str.encode('utf-8')
