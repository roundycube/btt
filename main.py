from fastapi import FastAPI

from base.urls import main_router

app = FastAPI()
app.include_router(main_router, prefix="")


